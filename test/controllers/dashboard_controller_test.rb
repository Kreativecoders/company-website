require 'test_helper'

class DashboardControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get portfolio_single_image" do
    get :portfolio_single_image
    assert_response :success
  end

  test "should get about" do
    get :about
    assert_response :success
  end

  test "should get contact1" do
    get :contact1
    assert_response :success
  end

  test "should get blog_timeline" do
    get :blog_timeline
    assert_response :success
  end

  test "should get service3" do
    get :service3
    assert_response :success
  end

end
