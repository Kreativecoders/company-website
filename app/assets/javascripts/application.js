// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs

//= require "jquery.parallax-1.1.3.js"
//= require "jquery.simple-text-rotator.js"
//= require "jquery.prettyPhoto.js"
//= require "jquery.themepunch.plugins.min.js"
//= require "jquery.themepunch.revolution.min.js"
//= require "jquery.easing-1.3.js"
//= require "jquery.royalslider.min.js"
//= require "jquery.flexslider.js"
//= require "jquery.jigowatt.js"
//= require "jquery.fitvids.js"
//= require "jquery.isotope.min.js"
//= require "bootstrap.min.js"
//= require "bootstrap-select.js"
//= require "menu.js"
//= require "owl.carousel.min.js"
//= require "wow.min.js"
//= require "custom.js"
//= require "custom-portfolio.js"
//= require "custom-portfolio-masonry.js"
//= require "countdown.js"
//= require "fswit.js"

